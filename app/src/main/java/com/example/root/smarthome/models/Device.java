package com.example.root.smarthome.models;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Device {

    public String deviceName;
    public int signal;

    // Default constructor required for calls to
    // DataSnapshot.getValue(Device.class)
    public Device() {
    }

    public Device(String deviceName, int signal) {
        this.deviceName = deviceName;
        this.signal = signal;
    }
}
