package com.example.root.smarthome;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private DatabaseReference mFirebaseDatabase_device;
    private DatabaseReference mFirebaseDatabase_info;
    private RadioGroup light1Group;
    private RadioButton light1On;
    private RadioButton light1Off;
    private RadioGroup light2Group;
    private RadioButton light2On;
    private RadioButton light2Off;
    private Button AC;
    private TextView tempValue;
    private TextView humiValue;
    int light1remotetoggle = 0;
    int light2remotetoggle = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);
        this.light1Group = findViewById(R.id.radio_1);
        this.light2Group = findViewById(R.id.radio_3);
        this.light1On = findViewById(R.id.light1On);
        this.light1Off = findViewById(R.id.light1Off);
        this.light2On = findViewById(R.id.light2On);
        this.light2Off = findViewById(R.id.light2Off);
        this.tempValue = findViewById(R.id.tempValue);
        this.humiValue = findViewById(R.id.humiValue);
        this.AC = findViewById(R.id.AC);
        light1On.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateDevice(true, "light 1", 1);
            }
        });
        light1Off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateDevice(true, "light 1", 0);
            }
        });
        light2On.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateDevice(true, "light 2", 1);
            }
        });
        light2Off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateDevice(true, "light 2", 0);
            }
        });
        /* Listening change from firebase */
        FirebaseDatabase mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase_device = mFirebaseInstance.getReference("devices");
        DatabaseReference acValue = mFirebaseInstance.getReference("devices").child("tv");
        AC.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                FirebaseDatabase mFirebaseInstance = FirebaseDatabase.getInstance();
                mFirebaseDatabase_device = mFirebaseInstance.getReference("devices");
                DatabaseReference acValue = mFirebaseInstance.getReference("devices").child("tv");
                acValue.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snap) {
                        if (Integer.parseInt(snap.getValue().toString()) == 0) {
                            updateDevice(true, "tv", 1);


                        } else {
                            updateDevice(true, "tv", 0);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }

                });
            }
        });

        mFirebaseDatabase_device.child("light 1").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int changeValue = Integer.valueOf(Objects.requireNonNull(dataSnapshot.getValue()).toString());
                if (changeValue == 1) {
                    light1Group.check(R.id.light1On);
                    light1remotetoggle = 1;
                } else {
                    light1Group.check(R.id.light1Off);
                    light1remotetoggle = 0;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        mFirebaseDatabase_device.child("light 2").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int changeValue = Integer.valueOf(Objects.requireNonNull(dataSnapshot.getValue()).toString());
                if (changeValue == 1) {
                    Log.d(TAG, "check on " + R.id.light2On);
                    light2Group.check(R.id.light2On);
                    light2remotetoggle = 1;
                } else {
                    Log.d(TAG, "check of " + R.id.light2Off);
                    light2Group.check(R.id.light2Off);
                    light2remotetoggle = 0;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

        mFirebaseDatabase_info = mFirebaseInstance.getReference("info");
        mFirebaseDatabase_info.child("humi").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String changeValue = Objects.requireNonNull(dataSnapshot.getValue()).toString();
                humiValue.setText(changeValue);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        mFirebaseDatabase_info.child("temperature").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String changeValue = Objects.requireNonNull(dataSnapshot.getValue()).toString();
                tempValue.setText(changeValue);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }


        });
        loadTabs();

    }

    private void updateDevice(boolean isDevice, String deviceName, int signal) {
        if (isDevice) {
            mFirebaseDatabase_device.child(deviceName).setValue(signal);
        } else {
            mFirebaseDatabase_info.child(deviceName).setValue(signal);
        }
    }

    public void loadTabs() {
        final TabHost tab = findViewById(R.id.tab_host);
        tab.setup();
        TabHost.TabSpec spec;
        spec = tab.newTabSpec("t1");
        spec.setContent(R.id.tab1);
        spec.setIndicator("Device");
        tab.addTab(spec);
        spec = tab.newTabSpec("t2");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Info");
        tab.addTab(spec);
        tab.setCurrentTab(0);
    }
}

